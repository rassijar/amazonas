<?php
<?php
$autor = new autor();
// $autores = $autor->consultartodos()
$cantidad = 5;
$pagina = 1;

$autores = $autor->consultarporpagina($cantidad, $pagina);
$totalregistros=$autor->consultartotalregistros();
?>





<div class="container">
	<div class="row mt-3">

		<div class="col">
			<div class="card">
				<div class="card-header">
					<h3>Consultar autor</h3>
				</div>
				<div class="card-body">
					<table class=" table table-striped table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>Nombre</th>
								<th>Apellido</th>
								<th>Servicios</th>
							</tr>
						</thead>
						<tbody>
							<?php

    $i = 1;

    foreach ($autores as $autorActual) {
        echo "<tr>";
        echo "<td>" . $i ++ . "</td><td>" . $autorActual->getNombre() . "</td><td>" . $autorActual->getApellido() . "</td>";
        echo "<td><a href='index.php?pid= " . base64_encode("presentacion/autor/editarAutor.php") . "&idAutor=" . $autorActual->getIdAutor() . "'><i class='fas fa-edit'></i></a></td>";
        echo "</tr>";
    }

    ?>
						</tbody>
					</table>
					<div class="row">
						<div class="col-10">
							<nav >
								<ul class="pagination">
									<li class="page-item <?php if($pagina==1){echo "disabled";}?>"><span class="page-link">Anterior</span>
									</li>
									<?php 
									for($i=1;$i<($totalregistros/$cantidad)+1;$i++){
									    
									    if($pagina == $i){
									        echo "<li class='page-item active'><span class='page-link'>" . $i . "</span></li>";
									    }else{ 
									    }"<li class='page-item active'><span class='page-link' href'#'>" . $i . "</span></li>";
									
									?>
									
									
									
									
									<li class="page-item"><a class="page-link" href="#">Siguiente</a></li>
								</ul>
							</nav>
						</div>
						<div class="col-2 text-right">
							<select name="cantidad" id="cantidad">
								<option value="5">5</option>
								<option value="10">10</option>
								<option value="20">20</option>
							</select>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>