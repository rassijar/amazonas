<?php 
$editado=false;

if (isset($_POST["editar"])){
    $autor= new autor($_GET["idAutor"], $_POST["nombre"],$_POST["apellido"]);
    $autor->editar();
    $editado=true;
}else{
    $autor= new autor($_GET["idAutor"]);
    $autor->consultar();
    
}


?>


<div class="container">
	<div class="row mt-3">
	<div class="col-3	"></div>
		<div class="col-6">
			<div class="card">
				<div class="card-header">
					<h3>Editar autor</h3>
				</div>
				<div class="card-body">
				<?php if ($editado) { ?>						
						<div class="alert alert-success alert-dismissible fade show"
							role="alert">
							<strong>Datos editados</strong>
							<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<?php } ?>
				
					<form
						action="<?php echo "index.php?pid=" . base64_encode("presentacion/autor/editarAutor.php"). "&idAutor=".$_GET["idAutor"] ?>"
						method="post">
						
						<div class="form-group">	
							<input type="text" name="nombre" class="form-control"
								placeholder="Nombre" value=" <?php echo $autor->getNombre()?>" required="required">
						</div>
						<div class="form-group">
							<input type="text" name="apellido" class="form-control"
								placeholder="Apellido" value=" <?php echo $autor->getNombre()?>" required="required">
						</div>
						<div class="form-group">
							<button type="submit" name="editar" class="btn btn-primary">Editar	</button>
						</div>
						<p>
							
						</p>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>