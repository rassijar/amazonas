<?php 
$creado=false;
if (isset($_POST["crear"])){
 $autor= new autor("", $_POST["nombre"],$_POST["apellido"]);
 $autor->crear();
 $creado=true;
}




?>


<div class="container">
	<div class="row mt-3">
	<div class="col-3	"></div>
		<div class="col-6">
			<div class="card">
				<div class="card-header">
					<h3>Crear autor</h3>
				</div>
				<div class="card-body">
				<?php if ($creado) { ?>						
						<div class="alert alert-success alert-dismissible fade show"
							role="alert">
							<strong>Usuario creado</strong>
							<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<?php } ?>
				
					<form
						action=<?php echo "index.php?pid=" . base64_encode("presentacion/autor/crearAutor.php") ?>
						method="post">
						
						<div class="form-group">	
							<input type="text" name="nombre" class="form-control"
								placeholder="Nombre" required="required">
						</div>
						<div class="form-group">
							<input type="text" name="apellido" class="form-control"
								placeholder="Apellido" required="required">
						</div>
						<div class="form-group">
							<button type="submit" name="crear" class="btn btn-primary">Crear</button>
						</div>
						<p>
							
						</p>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>