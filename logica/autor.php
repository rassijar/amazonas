<?php

require "persistencia/autorDAO.php";
class autor
{
    
    private $idAutor;
    private $nombre;
    private $apellido;
    private $conexion;
    private $autorDAO;
    
    
    
    
    
    
  

    /**
     * @return mixed
     */
    public function getIdAutor()
    {
        return $this->idAutor;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    function autor($pidAutor=" ", $pNombre=" ", $pApellido=" "){
        $this->idAutor=$pidAutor;
        $this->nombre=$pNombre;
        $this->apellido=$pApellido;
        
        $this->conexion= new conexion ();
        $this->autorDAO=new autorDAO($pidAutor, $pNombre, $pApellido);
        
    }
    
   
    function consultar(){
        
       $this->conexion->abrir();
       echo $this->autorDAO->consultar();
        $this->conexion->ejecutar($this->autorDAO->consultar());
       $this->conexion->cerrar();
        $resultado= $this->conexion->extraer();
        $this->nombre=$resultado[0];
        $this->apellido=$resultado[1];
      
       
        
    }
    function crear(){
        
        $this->conexion->abrir();

        $this->conexion->ejecutar($this->autorDAO->crear());
        $this->conexion->cerrar();
       
        
        
    }
    
    function consultartodos(){
        
        $this->conexion->abrir();
        
        $this->conexion->ejecutar($this->autorDAO->consultartodos());
        $this->conexion->cerrar();
        $autores= array();
        while (($resultado= $this->conexion->extraer())!=null){
            array_push($autores, new autor( $resultado[0], $resultado[1], $resultado[2]));
        }
        return $autores;
        
        
    }
    
    
    function editar(){
        
        $this->conexion->abrir();
        
        $this->conexion->ejecutar($this->autorDAO->editar());
        $this->conexion->cerrar();
        
        
        
    }
    
    function consultarporpagina($cantidad,$pagina){
        
        $this->conexion->abrir();
        echo $this->autorDAO->consultarporpagina($cantidad,$pagina);
        $this->conexion->ejecutar($this->autorDAO->consultarporpagina($cantidad,$pagina));
        $this->conexion->cerrar();
        $autores= array();
        while (($resultado= $this->conexion->extraer())!=null){
            array_push($autores, new autor( $resultado[0], $resultado[1], $resultado[2]));
        }
        return $autores;
        
        
    }
    
    function consultartotalregistros(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> autorDAO -> consultartotalregistros());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        echo  $resultado[0];
        return $resultado[0];
    }
}

?>
