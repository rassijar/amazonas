<?php
require_once "persistencia/conexion.php";
require "persistencia/administradorDAO.php";
class administrador
{

    private $idAdministrador;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $conexion;
    private $administradorDAO;
    
    
   
    public function getIdAdministrador()
    {
        return $this->idAdministrador;
    }

   
    public function getNombre()
    {
        return $this->nombre;
    }

    
    public function getApellido()
    {
        return $this->apellido;
    }

    
    public function getCorreo()
    {
        return $this->correo;
    }

    public function getClave()
    {
        return $this->clave;
    }

   
    public function getConexion()
    {
        return $this->conexion;
    }

    
    public function getAdministradorDAO()
    {
        return $this->administradorDAO;
    }

    function administrador($pidAdministrador=" ", $pnombre=" ", $papellido=" ", $pcorreo=" ", $pclave=" "){
        $this->idAdministrador=$pidAdministrador;
        $this->nombre=$pnombre;
        $this->apellido=$papellido;
        $this->correo=$pcorreo;
        $this->clave=$pclave;
        $this->conexion= new conexion ();
        $this->administradorDAO=new administradorDAO($pidAdministrador, $pnombre, $papellido, $pcorreo, $pclave);
        
    }
    
    function autenticar (){
        $this->conexion->abrir();
       
        $this->conexion->ejecutar($this->administradorDAO->autenticar());
        $this->conexion->cerrar();
        
        if($this->conexion->numfilas()==1){
           $this->idAdministrador= $this->conexion->extraer()[0];
           return true;
        }else{
            return false;
        }
        
        
        
    }
    
    function consultar(){
        
        $this->conexion->abrir();
        
        $this->conexion->ejecutar($this->administradorDAO->consultar());
        $this->conexion->cerrar();
        $resultado= $this->conexion->extraer();
        $this->nombre=$resultado[0];
          $this->apellido=$resultado[1];
          $this->correo=$resultado[2];
        
        
    }
        
}

?>
